//
//  ViewController.swift
//  homework2
//
//  Created by student on 10/15/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Block.1")
        print("Task.0")
        let f = Double(8128)
        let s = 9.45
        if let foo = getBiggerNumber(first: f, second: s) {
            print("\(foo) is the biggest value of [\(f) \(s)] ")
        }
        else {
            print("values are equal")
        }
        
        print("Task.1")
        print("the square of \(f) is \(pow2(value: f))")
        print("the cube of \(f) is \(f * pow2(value: f))")
        print("Task.2")
        print("sequence to \(f) - \(sequenceTo(value: Int(f)))")
        print("sequence from \(f) - \(sequenceTo(value: Int(f), reverse: true))")
        print("Task.3")
        print("count of divisors of \(f) is \(getDivisors(value: Int(f)).1)")
        print("\(f) divides without rest on \(getDivisors(value: Int(f)).0)")
        print("Task.4")
        if checkForPerfectness(value: Int(f)) {
            print("value \(Int(f)) is perfect")
        }
        else {
            print("value \(Int(f)) is not perfect")
        }
        
        print("Block.2")
        print("Task.1")
        print("they would have \(countProfit(start: Double(24), percent: Double(6), from: 1826))$")
        print("Task.2")
        print("The student must have \(countStudentYear(pricesGrowPercent: 3, salary: 700, needAtStart: 1000)) to stay alive")
        print("Task.3")
        print("The student will die in \(countStudentMonthsToDie(pricesGrowPercent: 3, salary: 700, needAtStart: 1000, avail: 2400)) months")
        print("Task.4")
        print("the reverse of 52 is \(reverseDigits(value: 52))")
    }
    
    func reverseDigits(value: Int) -> Int {
        let a1 = value % 10
        let a2 = (value - a1) / 10
        return a1 * 10 + a2
    }
    
    func countStudentMonthsToDie(pricesGrowPercent: Double, salary: Double, needAtStart: Double, avail: Double) -> Int {
        var money: Double = needAtStart
        var k = 0
        
        for i in 2...10 {
            money += needAtStart + needAtStart * pricesGrowPercent / 100.00
            if money >= avail + salary * Double(i) {
                k = i
                break
            }
        }
        
        return k
    }
    
    func countStudentYear(pricesGrowPercent: Double, salary: Double, needAtStart: Double) -> Double {
        var money: Double = needAtStart
        
        for _ in 2...10 {
            money += needAtStart + needAtStart * pricesGrowPercent / 100.00
        }
        
        return money - salary * 10
    }
    
    func countProfit(start capital: Double, percent: Double, from startYear: Int, to finishYear: Int = 2018) -> Double {
        var result = capital
        
        for _ in startYear...finishYear {
            result += (result / 100.00) * percent
        }
        
        return result
    }
    
    func checkForPerfectness(value: Int) -> Bool {
        let divisors = getDivisors(value: value).0
        var sum = 0;
        
        for i in divisors {
            if i == value {
                continue
            }
            sum += i
        }
        
        return sum == value
    }
    
    func getDivisors(value: Int) -> ([Int], Int) {
        var result: [Int] = []
        var count = 0
        
        for i in 1...value {
            let res = value % i
            if res == 0 {
                result.append(i)
                count += 1
            }
        }
        
        return (result, count)
    }
    
    func sequenceTo(value: Int, reverse: Bool = false) -> [Int] {
        var result: [Int] = []
        if !reverse {
            for i in 0...value {
                result.append(i)
            }
        }
        else {
            var i = value
            while i >= 0 {
                result.append(i)
                i -= 1
            }
        }
        
        return result
    }
    
    func getBiggerNumber(first: Double, second: Double) -> Double? {
        if first > second {
            return first
        }
        else if first == second {
            return nil
        }
        else {
            return second
        }
    }
    
    func pow2(value: Double) -> Double {
        return value * value
    }
}
